<?php

class CustomDate{

    const YEARDAYS = 280;
    const LEAPYEARDAYS = 279;

    private $dayLabels = array(
        "Mon",
        "Tue",
        "Wed",
        "Thu",
        "Fri",
        "Sat",
        "Sun"
    );
    
    private $monthLabels = array(
        '01' => "Month1",
        '02' => "Month2",
        '03' => "Month3",
        '04' => "Month4",
        '05' => "Month5",
        '06' => "Month6",
        '07' => "Month7",
        '08' => "Month8",
        '09' => "Month9",
        '10' => "Month10",
        '11' => "Month11",
        '12' => "Month12",
        '13' => "Month13"
    );

    function CustomDateAll($param, $date){

        $dateArr = explode('-', $date);
        $dateDay = $dateArr[0];
        $dateMonth = $dateArr[1];
        $dateYear = $dateArr[2];

        /* Number of Days in Month */
        if($param == 't'){
            return $this->numOfDays($dateMonth, $dateYear);
        }

        /* Selected Date */
        if($param == 'j'){
            return $dateDay;
        }

        /* Current Month Name */
        if($param == 'F'){
            return $this->monthLabels[$dateMonth];
        }

        /* Current Month Number */
        if($param == 'n'){
            return $dateMonth;
        }

        /* Current Year */
        if($param == 'Y'){
            return $dateYear;
        }

        /* Month Start Day */
        if($param == 'N'){
            return $this->monthStartDay($dateDay, $dateMonth, $dateYear);
        }

        /* Day */
        if($param == 'Z'){
            return $this->DateDay($dateDay, $dateMonth, $dateYear);
        }

    }

    function numOfDays($month, $year){
        
        if($month%2 == 0){
            return 21;
        }

        if($month%2 != 0 && $year%5 != 0){
            return 22;
        }

        if($month%2 != 0 && $year%5 == 0 && $month != 13){
            return 22;
        }

        if($month%2 != 0 && $year%5 == 0 && $month == 13){
            return 21;
        }

        
    }

    function monthStartDay($day, $month, $year){
        
        $count = 1;
        for($j = 1990; $j<= $year; $j++){
            for($k = 1; $k<=13; $k++ ){
                if(str_pad($k, 2, '0', STR_PAD_LEFT) == $month && $j == $year){
                    break 2;
                }
                $count += $this->numOfDays($k, $j);
            }
        }
        //$count += $day;

        
        $dayNumber = $count%7;
        if($dayNumber == 1){
            return 0;
        }elseif($dayNumber == 2){
            return 1;
        }elseif($dayNumber == 3){
            return 2;
        }elseif($dayNumber == 4){
            return 3;
        }elseif($dayNumber == 5){
            return 4;
        }elseif($dayNumber == 6){
            return 5;
        }else{
            return 6;
        }
    }


    function DateDay($day, $month, $year){
        
        $count = 1;
        for($j = 1990; $j<= $year; $j++){
            for($k = 1; $k<=13; $k++ ){
                if(str_pad($k, 2, '0', STR_PAD_LEFT) == $month && $j == $year){
                    break 2;
                }
                $count += $this->numOfDays($k, $j);
            }
        }
        
        $count += $day;
        
        $dayNumber = $count%7;
        if($dayNumber == 1){
            return "Sunday";
        }elseif($dayNumber == 2){
            return "Monday";
        }elseif($dayNumber == 3){
            return "Tuesday";
        }elseif($dayNumber == 4){
            return "Wednesday";
        }elseif($dayNumber == 5){
            return "Thursday";
        }elseif($dayNumber == 6){
            return "Friday";
        }else{
            return "Saturday";
        }
    }
}