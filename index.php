<?php
include 'date.php';

class Calendar
{
	var $events;

	function Calendar($date)
	{
        $d = new CustomDate();

		if(empty($date)) {
            $date = '01-01-1990';
        }
		define('NUM_OF_DAYS', $d->CustomDateAll('t',$date));
		define('CURRENT_DAY', $d->CustomDateAll('j',$date));
		define('CURRENT_MONTH_A', $d->CustomDateAll('F',$date));
		define('CURRENT_MONTH_N', $d->CustomDateAll('n',$date));
		define('CURRENT_YEAR', $d->CustomDateAll('Y',$date));
		define('START_DAY', $d->CustomDateAll('N', $date));
		define('COLUMNS', 7);
		define('PREV_MONTH', $this->prev_month());
		define('NEXT_MONTH', $this->next_month());
		
	}

	function prev_month()
	{

        if(CURRENT_MONTH_N == 01){
            $getMonth = 13;
            $getMonth = str_pad($getMonth, 2, '0', STR_PAD_LEFT);
        }else{
            $getMonth = CURRENT_MONTH_N - 1;
            $getMonth = str_pad($getMonth, 2, '0', STR_PAD_LEFT);
        }

        if(CURRENT_MONTH_N == 01){
            $getYear = CURRENT_YEAR - 1;
        }else{
            $getYear = CURRENT_YEAR;
        }


		return '01-'.$getMonth.'-'.$getYear;
	}
	
	function next_month()
	{
        if(CURRENT_MONTH_N == 13){
            $getMonth = 1;
            $getMonth = str_pad($getMonth, 2, '0', STR_PAD_LEFT);
        }else{
            $getMonth = CURRENT_MONTH_N + 1;
            $getMonth = str_pad($getMonth, 2, '0', STR_PAD_LEFT);
        }

        if(CURRENT_MONTH_N == 13){
            $getYear = CURRENT_YEAR + 1;
        }else{
            $getYear = CURRENT_YEAR;
		}
		
		return '01-'.$getMonth.'-'.$getYear;
	}
	

	
	function makeImaginaryCalendar()
	{
		echo '<table border="1" cellspacing="4"><tr>';
		if(CURRENT_YEAR == 1990 && CURRENT_MONTH_N == 1){
			echo '<td width="30"></td>';
		}else{
			echo '<td width="30"><a href="?date='.PREV_MONTH.'">&lt;&lt;</a></td>';
		}
		echo '<td colspan="5" style="text-align:center">'.CURRENT_MONTH_A .' - '. CURRENT_YEAR.'</td>';
		echo '<td width="30"><a href="?date='.NEXT_MONTH.'">&gt;&gt;</a></td>';
		echo '</tr><tr>';
		echo '<td width="30">Mon</td>';
		echo '<td width="30">Tue</td>';
		echo '<td width="30">Wed</td>';
		echo '<td width="30">Thu</td>';
		echo '<td width="30">Fri</td>';
		echo '<td width="30">Sat</td>';
		echo '<td width="30">Sun</td>';
		echo '</tr><tr>';
		
		echo str_repeat('<td>&nbsp;</td>', START_DAY);
		
		$rows = 1;
		
		for($i = 1; $i <= NUM_OF_DAYS; $i++)
		{
			if($i == CURRENT_DAY)
				echo '<td style="background-color: #C0C0C0"><strong>'.$i.'</strong></td>';
			else
                echo '<td><a href="?date='.str_pad($i, 2, '0', STR_PAD_LEFT).'-'.CURRENT_MONTH_N.'-'.CURRENT_YEAR.'">'.$i.'</a></td>';
            
					
			if((($i + START_DAY) % COLUMNS) == 0 && $i != NUM_OF_DAYS)
			{
				echo '</tr><tr>';
				$rows++;
			}
		}
		echo str_repeat('<td>&nbsp;</td>', (COLUMNS * $rows) - (NUM_OF_DAYS + START_DAY)).'</tr></table>';
	}
}

//echo date('N',$_GET['date']);

if(isset($_GET['date'])){
    $paramDate = $_GET['date'];
}else{
    $paramDate = '01-01-1990';
}
$cal = new Calendar($paramDate);
$cal->makeImaginaryCalendar();

$d = new CustomDate();
echo $d->CustomDateAll('Z', $paramDate);

?>